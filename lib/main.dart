import "package:flutter/material.dart";

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "A simple counter",
      theme: ThemeData(
        primaryColor: Colors.blue,
        accentColor: Colors.green,
      ),
      home: Counter(),
    );
  }
}

class Counter extends StatefulWidget {
  State<Counter> createState() => CounterState();
}

class CounterState extends State<Counter> {
  int _counter = 0;
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("A counting app..."),
      ),
      body: Center(child: Column(children: <Widget>[
        Expanded(child: Center(child: Text("... because counting is that hard"))),
        Expanded(child: Center(child: Text("Counter is ${this._counter}"))),
      ])),
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.add,
        ),
        onPressed: () => this.setState(() => ++this._counter),
      ),
    );
  }
}
